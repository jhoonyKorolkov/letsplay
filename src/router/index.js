import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Login from '@/views/Login.vue';
import Profile from '@/views/Profile.vue';
import Reg from '@/views/Reg.vue';
import ChoosePlayers from '@/views/ChoosePlayers.vue';
import PlayGround from '@/views/PlayGround.vue';
import PlayRoom from '@/views/PlayRoom.vue';
import { getStorage } from '@/api/localStorage';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/registration',
    name: 'Reg',
    component: Reg,
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      requiredLogin: true,
    },
  },
  {
    path: '/choose',
    name: 'ChoosePlayers',
    component: ChoosePlayers,
  },
  {
    path: '/playground',
    name: 'PlayGround',
    component: PlayGround,
  },
  {
    path: '/playroom',
    name: 'PlayRoom',
    component: PlayRoom,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const userIsOnline = getStorage('token');

  if (to.meta.requiredLogin) {
    if (userIsOnline) {
      next();
    } else next('/login');
  }

  next();
});

export default router;

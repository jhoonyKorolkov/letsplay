export default {
  namespaced: true,

  state: {
    loading: false,
  },

  getters: {
    isLoading: (state) => state.loading,
  },

  mutations: {
    SET_LAODING(state, payload) {
      state.loading = payload;
    },
  },

  actions: {
    setLoader: {
      root: true,
      handler({ commit }, payload) {
        commit('SET_LAODING', payload);
      },
    },
  },
};

export default {
  namespaced: true,

  state: {
    error: null,
  },

  getters: {
    isError: (state) => state.error,
  },

  mutations: {
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    CLEAR_ERROR(state) {
      state.error = null;
    },
  },

  actions: {
    setGlobalError: {
      root: true,
      handler({ commit }, payload) {
        commit('SET_ERROR', payload);
      },
    },

    clearGlobalError: {
      root: true,
      handler({ commit }) {
        commit('CLEAR_ERROR');
      },
    },
  },
};

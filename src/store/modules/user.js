/* eslint-disable import/no-extraneous-dependencies */
import firebase from 'firebase/app';
import { db } from '@/api/firebase';
import sha256 from 'crypto-js/sha256';
import { setStorage, getStorage, clearStorage } from '@/api/localStorage';

export default {
  namespaced: true,
  state: {
    token: null,
    name: null,
    nickname: null,
    email: null,
    id: null || getStorage('user-id'),
    usersReg: [],
    users: [],
  },
  getters: {
    userToken: (state) => state.token || getStorage('token'),
    userName: (state) => state.name,
    userEmail: (state) => state.email,
    userId: (state) => state.id || getStorage('user-id'),
    allUsers: (state) => state.users,
  },

  mutations: {
    SET_USER_TOKEN(state, uid) {
      state.token = uid;
    },
    SET_USER_NAME(state, name) {
      state.name = name;
    },
    SET_USER_EMAIL(state, email) {
      state.email = email;
    },
    SET_USER_ID(state, id) {
      state.id = id;
    },
    SET_REGISTERED_USERS(state, payload) {
      state.usersReg = payload;
    },
    SET_ALL_USERS(state, payload) {
      const admin = state.usersReg.filter((el) => el.id === state.id);
      state.users = [...admin, ...payload];
    },
    SET_USER_TO_USERS(state, payload) {
      state.users.push(payload);
    },
    REMOVE_USER_IN_USERS(state, payload) {
      const idx = state.users.indexOf(payload);
      state.users.splice(idx, 1);
    },

    CLEAR_USER(state) {
      state.token = null;
    },
  },

  actions: {
    async registerUser({ commit, dispatch }, { email, password }) {
      dispatch('setLoader', true, { root: true });
      try {
        await firebase.auth().createUserWithEmailAndPassword(email, password);
        const id = sha256(email)
          .toString()
          .substr(45);

        commit('SET_USER_EMAIL', email);
        commit('SET_USER_ID', id);

        setStorage('user-id', id);

        dispatch('clearGlobalError', true, { root: true });
        dispatch('setLoader', false, { root: true });
        return true;
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
        return false;
      }
    },

    async addUser({ commit, dispatch, getters }, { name, id }) {
      dispatch('setLoader', true, { root: true });

      try {
        const document = db.collection('users').doc(getters.userId);
        await document.set({
          name,
          id: getters.userId,
        });

        dispatch('setGlobalError', 'Поздравляю, вы успешно зарегистрировались!', { root: true });
        dispatch('setLoader', false, { root: true });
        return true;
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
        return false;
      }
    },

    async loginUser({ commit, dispatch, getters }, { email, password }) {
      dispatch('setLoader', true, { root: true });
      try {
        const auth = await firebase.auth().signInWithEmailAndPassword(email, password);
        const id = sha256(email)
          .toString()
          .substr(45);

        commit('SET_USER_TOKEN', auth.user.uid);
        commit('SET_USER_ID', id);

        setStorage('token', auth.user.uid);
        setStorage('user-id', id);

        dispatch('clearGlobalError', true, { root: true });
        dispatch('setLoader', false, { root: true });
        return true;
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
        return false;
      }
    },

    async logoutUser({ commit }) {
      await firebase.auth().signOut();
      clearStorage('token');
      clearStorage('players');
      clearStorage('user-id');
      commit('CLEAR_USER');
    },

    async loadUser({ commit, dispatch, getters }) {
      if (!getters.userId) return;

      dispatch('setLoader', true, { root: true });
      try {
        const doc = await db
          .collection('users')
          .doc(getters.userId)
          .get();

        if (doc.exists) {
          commit('SET_USER_NAME', doc.data().name);
        }

        dispatch('clearGlobalError', true, { root: true });
        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async getRegisteredUsers({ commit, dispatch }) {
      dispatch('setLoader', true, { root: true });

      try {
        const snapshot = await db.collection('users').get();
        const result = snapshot.docs.map((doc) => doc.data());
        commit('SET_REGISTERED_USERS', result);

        dispatch('clearGlobalError', true, { root: true });
        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async getlocalUsers({ commit, dispatch, getters }) {
      dispatch('setLoader', true, { root: true });

      try {
        const snapshot = await db.collection(`usersLocal_${getters.userId}`).get();
        const result = snapshot.docs.map((doc) => doc.data());
        commit('SET_ALL_USERS', result);

        dispatch('clearGlobalError', true, { root: true });
        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async saveNewUsers({ commit, dispatch, getters }, payload) {
      dispatch('setLoader', true, { root: true });

      try {
        const batch = await db.batch();

        payload.forEach((doc) => {
          const id = sha256(doc.name)
            .toString()
            .substr(45);
          const docRef = db.collection(`usersLocal_${getters.userId}`).doc(id);
          doc.id = id;
          batch.set(docRef, doc);
          commit('SET_USER_TO_USERS', doc);

          dispatch('clearGlobalError', true, { root: true });
        });

        await batch.commit();

        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async deleteUser({ commit, dispatch, getters }, payload) {
      try {
        await db
          .collection(`usersLocal_${getters.userId}`)
          .doc(payload.id)
          .delete();

        commit('REMOVE_USER_IN_USERS', payload);
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },
  },
};

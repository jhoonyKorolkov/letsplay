/* eslint-disable import/no-extraneous-dependencies */
import { db } from '@/api/firebase';
import sha256 from 'crypto-js/sha256';

export default {
  namespaced: true,

  state: {
    players: [],
    playersGroup: [],
    commands: [],
    gameCouple: {},
    resultCouple: [],
    isError: false,
    groupId: null,
  },

  getters: {
    players: (state) => state.players,
    playersGroup: (state) => state.playersGroup,
    commands: (state) => state.commands,
    gameCouple: (state) => state.gameCouple,
    resultCouple: (state) => state.resultCouple,
    groupId: (state) => state.groupId,

    isError: (state) => state.isError,
  },

  mutations: {
    SET_PLAYERS(state, payload) {
      state.players = [];
      state.players = payload;
    },

    SET_PLAYERS_GROUP(state, payload) {
      state.playersGroup = payload;
    },

    SET_COMMANDS(state, payload) {
      state.commands = payload;
    },

    SET_COMMAND_TO_COMMANDS(state, payload) {
      state.commands.push(payload);
    },

    REMOVE_COMMAND_IN_COMMANDS(state, payload) {
      const idx = state.commands.indexOf(payload);
      state.commands.splice(idx, 1);
    },

    SET_GAME_COUPLE(state, payload) {
      state.gameCouple = payload;
    },

    SET_RESULT_GAME_COUPLE(state, payload) {
      // console.log(state.playersGroup);
      // Object.values(state.playersGroup).forEach((el) => {
      //   console.log(el);
      // });

      console.log(state.playersGroup);
      // state.resultCouple.push(payload);
    },

    SET_GROUP_ID(state, payload) {
      state.groupId = payload;
    },

    SET_ERROR(state, payload) {
      payload.forEach((el) => {
        if (!el.command) {
          el.error = 'Выберите команду';
          state.isError = true;
        }
      });
    },

    OPEN_SELECT(state, payload) {
      payload.active = !payload.active;
    },

    REMOVE_ERROR(state, payload) {
      delete payload.error;
      state.isError = false;
    },
  },

  actions: {
    async saveNewCommands({ commit, dispatch }, payload) {
      dispatch('setLoader', true, { root: true });

      try {
        const batch = await db.batch();

        payload.forEach((doc) => {
          const id = sha256(doc.name)
            .toString()
            .substr(45);
          const docRef = db.collection('commands').doc(id);
          doc.id = id;
          batch.set(docRef, doc);
          commit('SET_COMMAND_TO_COMMANDS', doc);
        });

        await batch.commit();

        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async deleteCommand({ commit, dispatch }, payload) {
      try {
        await db
          .collection('commands')
          .doc(payload.id)
          .delete();

        commit('REMOVE_COMMAND_IN_COMMANDS', payload);
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async getCommands({ commit, dispatch }) {
      dispatch('setLoader', true, { root: true });
      commit('SET_COMMANDS', []);

      const result = [];
      try {
        const data = await db.collection('commands').get();
        data.forEach((doc) => {
          result.push(doc.data());
        });
        commit('SET_COMMANDS', result);

        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', 'error.message', { root: true });
      }
    },

    async createPlayersGroup({ commit, dispatch, rootGetters }, payload) {
      dispatch('setLoader', true, { root: true });

      commit('SET_PLAYERS_GROUP', []);
      try {
        await db
          .collection(`gameGroup_${rootGetters['user/userId']}`)
          .doc('PG')
          .set({
            group: payload,
          });
        commit('SET_PLAYERS_GROUP', payload);

        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async loadPlayersGroup({ dispatch, commit, rootGetters }, payload) {
      dispatch('setLoader', true, { root: true });

      try {
        const doc = await db
          .collection(`gameGroup_${rootGetters['user/userId']}`)
          .doc('PG')
          .get();
        if (doc.exists) {
          commit('SET_PLAYERS_GROUP', doc.data().group);
          dispatch('setLoader', false, { root: true });
        }

        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    async updateGroup({ dispatch, getters, rootGetters }, payload) {
      dispatch('setLoader', true, { root: true });

      try {
        const result = await db.collection(`gameGroup_${rootGetters['user/userId']}`).doc('PG');
        result.update({ [`group.${getters.groupId}`]: [...payload] });

        dispatch('setLoader', false, { root: true });
      } catch (error) {
        dispatch('setLoader', false, { root: true });
        dispatch('setGlobalError', error.message, { root: true });
      }
    },

    savePlayers({ commit }, payload) {
      commit('SET_PLAYERS', payload.users);
      commit('SET_COMMANDS', payload.commands);
    },

    startGame({ commit }, payload) {
      commit('SET_GAME_COUPLE', payload.couple);
      commit('SET_GROUP_ID', payload.keyGroup);
    },
  },
};

import Vue from 'vue';
import Vuex from 'vuex';

import error from './modules/error';
import loader from './modules/loader';
import user from './modules/user';
import players from './modules/players';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    loader,
    error,
    user,
    players,
  },
  strict: process.env.NODE_ENV !== 'production',
});

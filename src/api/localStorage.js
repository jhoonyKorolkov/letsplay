function setStorage(key, value) {
  return localStorage.setItem(key, JSON.stringify(value));
}

function getStorage(key) {
  return JSON.parse(localStorage.getItem(key));
}

function clearStorage(key) {
  return localStorage.removeItem(key);
}

export { setStorage, getStorage, clearStorage };

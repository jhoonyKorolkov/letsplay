/* eslint-disable import/no-extraneous-dependencies */
import firebase from 'firebase/app';

import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: 'letsplay-73d46.firebaseapp.com',
  projectId: 'letsplay-73d46',
  storageBucket: 'letsplay-73d46.appspot.com',
  messagingSenderId: '490508236293',
  appId: '1:490508236293:web:434fa594abf9fff00ace6c',
};

const firebaseInit = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export { firebaseInit, db };

import Vue from 'vue';
import router from './router';
import store from './store';
import App from './App.vue';

import { firebaseInit } from './api/firebase';

Vue.config.productionTip = false;

new Vue({
  firebaseInit,
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

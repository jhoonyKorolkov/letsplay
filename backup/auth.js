import { logToServer, axios } from '@/api/server';

export default {
  namespaced: true,

  state: {
    email: null,
    isLogin: false,
    name: null,
  },

  getters: {
    userEmail: (state) => state.email,
    userName: (state) => state.name,
    isLogin: (state) => state.isLogin,
  },

  mutations: {
    SAVE_USER_EMAIL(state, email) {
      state.email = email;
      state.isLogin = true;
    },
    LOAD_USER(state) {
      state.isLogin = true;
    },
    LOG_OUT_USER(state) {
      state.email = '';
      state.isLogin = false;
    },
    SAVE_USER_NAME(state, userName) {
      state.name = userName;
    },
  },

  actions: {
    async sendData(store, { email, password }) {
      store.dispatch('setLoader', true, { root: true });
      try {
        const res = await logToServer(email, password);
        localStorage.setItem('userToken', JSON.stringify(res.token));
        store.commit('SAVE_USER_EMAIL', email);
        store.dispatch('setLoader', false, { root: true });
        return true;
      } catch (error) {
        store.dispatch(
          'setIsError',
          'Пользователя с такими данными не существует, проверьте логин и пароль',
          { root: true },
        );
        store.dispatch('setLoader', false, { root: true });
        return false;
      }
    },

    async getUserName(store) {
      if (store.getters.isLogin === false) return;
      store.dispatch('setLoader', true, { root: true });
      try {
        const userToken = JSON.parse(localStorage.getItem('userToken'));
        const result = await axios.getUserName({
          headers: {
            authorization: `Bearer ${userToken}`,
          },
        });
        store.commit('SAVE_USER_NAME', result.name);
        store.dispatch('setLoader', false, { root: true });
        localStorage.setItem('userName', JSON.stringify(result.name));
      } catch (error) {
        store.dispatch('setIsError', 'Пользователь не существует', { root: true });
      }
    },

    logOut({ commit }) {
      localStorage.removeItem('userToken');
      localStorage.removeItem('userName');
      commit('LOG_OUT_USER');
    },

    loadUser({ commit }) {
      const token = localStorage.getItem('userToken');
      const name = JSON.parse(localStorage.getItem('userName'));
      if (token && name) {
        commit('LOAD_USER');
        commit('SAVE_USER_NAME', name);
      }
    },
  },
};

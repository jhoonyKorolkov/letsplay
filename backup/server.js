/**
 *
 * @param {String} email - your email to login
 * @param {String} password - your password to login
 */

function logToServer(email, password) {
  const timer = Math.floor(Math.random() * 3000) + 1000;
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // validations
      if (!email) reject(Error('Email is required'));
      if (!password) reject(Error('Password is required'));
      if (email !== 'johny@gmail.com') reject(Error('email is wrong'));
      if (password !== 'qweqwe') reject(Error('Password is wrong'));
      const token = `id${Math.random()
        .toString(16)
        .slice(2)}`;
      resolve({ token });
    }, timer);
  });
}

const axios = {
  /**
   * @param {String} url - must be '/getuser'
   * @param { { headers: { authorization: String } } } options - request options
   * @returns
   */
  async get(url, options) {
    const timer = Math.floor(Math.random() * 3000) + 1000;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (url !== '/profile') reject(Error('Bad request'));

        const { headers } = options;
        const { authorization } = headers || {};

        if (!authorization) reject(Error('Unauthorized'));
        if (!authorization.includes('Bearer ')) reject(Error('Unauthorized'));

        const token = authorization.split(' ')[1];
        const toCompareToken = JSON.parse(window.localStorage.getItem('userToken'));

        const isSame = token === toCompareToken;
        if (!isSame) reject(Error('Unauthorized'));

        if (isSame) {
          resolve({
            name: 'Joohny',
            email: 'webwork@gmail.com',
          });
        }
        reject(Error('Unexpected error'));
      }, timer);
    });
  },

  async getUserName(options) {
    const timer = Math.floor(Math.random() * 3000) + 1000;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const { headers } = options;
        const { authorization } = headers || {};

        if (!authorization) reject(Error('Unauthorized'));
        if (!authorization.includes('Bearer ')) reject(Error('Unauthorized'));

        const token = authorization.split(' ')[1];
        const toCompareToken = JSON.parse(window.localStorage.getItem('userToken'));

        const isSame = token === toCompareToken;
        if (!isSame) reject(Error('Unauthorized'));

        if (isSame) {
          resolve({
            name: 'Joohny',
          });
        }
        reject(Error('Unexpected error'));
      }, timer);
    });
  },
};

export { logToServer, axios };
